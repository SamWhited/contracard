## Submitting Patches

Patches may be submitted on [Codeberg].

[Codeberg]: https://codeberg.org/SamWhited/contracard


## License

Licensed under the LaTeX Project Public License (LPPL).
See the [`LICENSE`] file for details.

[`LICENSE`]: https://codeberg.org/SamWhited/contracard/src/branch/main/LICENSE


### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you shall be licensed as above, without any
additional terms or conditions.

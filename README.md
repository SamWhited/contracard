# Contra Card

A LaTeX package for typesetting traditional contra and square dances, and a
class which generates calling cards.

## Download

Development of this package occurs primarily on [Codeberg].
Patches should be submitted there.

The current [release version] of Contra Card and associated [documentation] is
available from [CTAN].

[Codeberg]: https://codeberg.org/SamWhited/contracard
[release version]: http://ctan.org/tex-archive/macros/latex/contrib/contracard
[documentation]: https://codeberg.org/SamWhited/contracard
[CTAN]: http://ctan.org/pkg/contracard
